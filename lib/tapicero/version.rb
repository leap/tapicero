module Tapicero
  VERSION = "0.4.1"
  REQUIRE_PATHS = ['lib']
  FLAGS = []
  CONFIGS = ['config/default.yaml', '/etc/leap/tapicero.yaml']
end
